       identification division.
       program-id. project.
       author. Will Orr.

       environment division.
       input-output section.
       file-control.
      * Quotes needed to give full path to files
           select input-file assign to "./number-file"
               organization is line sequential.
      * Organization needs to be line sequential to read properly
           select output-file assign to "./sum-file".

       data division.
       file section.
       fd input-file.
       01 input-rec.
           05 first-number pic 9(2).
           05 filler pic x.
      * Filler required to account for space
           05 second-number pic 9(2).

       fd output-file.
       01 output-rec pic x(80).

       working-storage section.
       01 switches.
           05 eof-switch pic x value "n".

       01 print-fields.
           05 filler pic x(11) value "The sum of ".
           05 first-field pic z(2).
           05 filler pic x(5) value " and ".
           05 second-field pic z(2).
           05 filler pic x(4) value " is ".
           05 sum-field pic z(3).

       01 work-fields.
           05 sum-number pic 9(3).

       procedure division.

       000-main-procedure.
           open input input-file output output-file.
           perform 100-calculate-sums until eof-switch = "y".
           close input-file output-file.
           stop run.

       100-calculate-sums.
           perform 110-read-input-file.
           compute sum-number = first-number + second-number.
           perform 120-write-out.

       110-read-input-file.
           read input-file at end
               move "y" to eof-switch.

       120-write-out.
           move first-number to first-field.
           move second-number to second-field.
           move sum-number to sum-field.
           move print-fields to output-rec.
           write output-rec before advancing 1 lines.
